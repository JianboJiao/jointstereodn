// Demo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;
#include <opencv2/opencv.hpp>
using namespace cv;

struct pt
{
	int x;
	int y;
	double dist;
};
inline bool SortByDist(const pt &v1, const pt &v2)
{
	return v1.dist > v2.dist;
}

vector<Mat> AD(Mat left, Mat right, int dr)
{
	int hei = left.rows;
	int wid = left.cols;
	vector<Mat> cost(dr);
	Mat leftg, rightg;
	if (left.channels() == 3)
	{
		cvtColor(left, leftg, CV_BGR2GRAY);
		cvtColor(right, rightg, CV_BGR2GRAY);
		left.convertTo(left, CV_32FC3);
		right.convertTo(right, CV_32FC3);
	}
	else
	{
		left.convertTo(left, CV_32F);
		right.convertTo(right, CV_32F);
	}
	Mat AD;
	Mat right_b;
	copyMakeBorder(right, right_b, 0, 0, dr, 0, BORDER_REFLECT);
	Mat Cens;
	if (left.channels() == 3)
	{
		vector<Mat> chanlsl, chanlsr;
		split(left, chanlsl);
		split(right_b, chanlsr);
		Mat rightg_b;
		copyMakeBorder(rightg, rightg_b, 0, 0, dr, 0, BORDER_REFLECT);
		for (int d = 1; d <= dr; ++d)
		{
			AD = Mat::zeros(hei, wid, CV_32F);
			for (int c = 0; c < left.channels(); ++c)
			{
				Mat tmp = chanlsr[c].colRange(dr - d, dr - d + wid);
				AD = AD + abs(chanlsl[c] - tmp);
			}
			AD /= 3.0;

			normalize(AD, AD, 1.0, 0.0, NORM_MINMAX);
			cost[d - 1] = AD.clone();
			normalize(cost[d - 1], cost[d - 1], 1.0, 0.0, NORM_MINMAX);
		}
	}
	else
	{
		for (int d = 1; d <= dr; ++d)
		{
			AD = Mat::zeros(hei, wid, CV_32F);
			Mat tmp = right_b.colRange(dr - d, dr - d + wid);
			AD = abs(left - tmp);

			normalize(AD, AD, 1.0, 0.0, NORM_MINMAX);
			cost[d - 1] = AD.clone();
			normalize(cost[d - 1], cost[d - 1], 1.0, 0.0, NORM_MINMAX);
		}
	}

	return cost;
}
vector<Mat> AD_r2l(Mat left, Mat right, int dr)
{
	int hei = left.rows;
	int wid = left.cols;
	vector<Mat> cost(dr);
	Mat leftg, rightg;
	if (left.channels() == 3)
	{
		cvtColor(left, leftg, CV_BGR2GRAY);
		cvtColor(right, rightg, CV_BGR2GRAY);
		left.convertTo(left, CV_32FC3);
		right.convertTo(right, CV_32FC3);
	}
	else
	{
		left.convertTo(left, CV_32F);
		right.convertTo(right, CV_32F);
		left.copyTo(leftg);
		right.copyTo(rightg);
	}
	Mat AD;
	Mat left_b;
	copyMakeBorder(left, left_b, 0, 0, 0, dr, BORDER_REFLECT);

	if (left.channels() == 3)
	{
		vector<Mat> chanlsl, chanlsr;
		split(left_b, chanlsl);
		split(right, chanlsr);
		for (int d = 1; d <= dr; ++d)
		{
			AD = Mat::zeros(hei, wid, CV_32F);
			for (int c = 0; c < left.channels(); ++c)
			{
				Mat tmp = chanlsl[c].colRange(d, d + wid);
				AD = AD + abs(chanlsr[c] - tmp);
			}
			AD /= 3.0;

			normalize(AD, AD, 1.0, 0.0, NORM_MINMAX);
			cost[d - 1] = AD.clone();
			normalize(cost[d - 1], cost[d - 1], 1.0, 0.0, NORM_MINMAX);
		}
	}
	else
	{
		for (int d = 1; d <= dr; ++d)
		{
			AD = Mat::zeros(hei, wid, CV_32F);
			Mat tmp = left_b.colRange(d, d + wid);
			AD = abs(right-tmp);

			normalize(AD, AD, 1.0, 0.0, NORM_MINMAX);
			cost[d - 1] = AD.clone();
			normalize(cost[d - 1], cost[d - 1], 1.0, 0.0, NORM_MINMAX);
		}
	}

	return cost;
}
Mat AggrBox(vector<Mat> cost, int ws)
{
	int dr = cost.size();
	int hei = cost[0].rows;
	int wid = cost[0].cols;
	Mat disp(hei, wid, CV_8U);
	Mat tmp;
	for (int d = 1; d <= dr; ++d)
	{
		blur(cost[d-1], tmp, Size(ws, ws));
		cost[d-1] = tmp.clone();
	}
	for (int i = 0; i < hei; ++i)
		for (int j = 0; j < wid; ++j)
		{
			float dmin = FLT_MAX;
			int dij = 0;
			for (int d = 1; d <= dr; ++d)
			{
				if (cost[d-1].at<float>(i, j) < dmin)
				{
					dmin = cost[d-1].at<float>(i, j);
					dij = d;
				}
			}
			disp.at<uchar>(i, j) = dij;
		}
	return disp;
}
Mat NLM_multiv_PCA_2v_needle(Mat left, Mat right, Mat disp, int k, int S, int sigma, int clsnum, int PCANum, string flg)
{
	int height = left.rows;
	int width = left.cols;
	int ka = (k - 1) / 2;
	int Sa = (S - 1) / 2;
	double h = 6 * sigma + 14;
	Mat est = Mat::zeros(height, width, left.type());
	Mat den = Mat::zeros(height, width, CV_32F);
	Mat num = Mat::zeros(height, width, CV_32F);

	/*****************************************************************************************************/
	Mat PatAsRow((height - 2 * ka)*(width - 2 * ka) * 2, k*k, CV_32F);
	for (int i = ka; i < height - ka; ++i)
		for (int j = ka; j < width - ka; ++j)
		{
			Mat pat(left, Rect(j - ka, i - ka, k, k));
			pat.convertTo(pat, CV_32F);
			Mat pat_t = pat.clone();
			pat_t.reshape(0, 1).copyTo(PatAsRow.row((i - ka)*(width - 2 * ka) + j - ka));
		}
	for (int i = ka; i < height - ka; ++i)
		for (int j = ka; j < width - ka; ++j)
		{
			Mat pat(right, Rect(j - ka, i - ka, k, k));
			pat.convertTo(pat, CV_32F);
			Mat pat_t = pat.clone();
			pat_t.reshape(0, 1).copyTo(PatAsRow.row((height - 2 * ka)*(width - 2 * ka) + (i - ka)*(width - 2 * ka) + j - ka));
		}
	PCA pca(PatAsRow, Mat(), CV_PCA_DATA_AS_ROW, PCANum);
	PatAsRow.release();

	Mat leftPAR = Mat::zeros((height - 2 * ka)*(width - 2 * ka), k*k, CV_32F);
	Mat rightPAR = Mat::zeros((height - 2 * ka)*(width - 2 * ka), k*k, CV_32F);

	for (int i = ka; i < height - ka; ++i)
		for (int j = ka; j < width - ka; ++j)
		{
			Mat pat(left, Rect(j - ka, i - ka, k, k));
			pat.convertTo(pat, CV_32F);
			Mat pat_t = pat.clone();
			pat_t.reshape(0, 1).copyTo(leftPAR.row((i - ka)*(width - 2 * ka) + j - ka));
			Mat pat2(right, Rect(j - ka, i - ka, k, k));
			pat2.convertTo(pat2, CV_32F);
			Mat pat_t2 = pat2.clone();
			pat_t2.reshape(0, 1).copyTo(rightPAR.row((i - ka)*(width - 2 * ka) + j - ka));
		}
	Mat dst_left = pca.project(leftPAR);
	Mat dst_right = pca.project(rightPAR);
	leftPAR.release();
	rightPAR.release();
	/*****************************************************************************************************/

	vector<vector<Mat>> needle(2,vector<Mat>(8));
	vector<Mat> img(2), dstimg(2);
	double sl[8];
	if (flg == "left")
	{
		for (int n = 0; n < 8; ++n)
		{
			sl[n] = pow(0.75, n);
			resize(left, needle[0][n], Size(), sl[n], sl[n], INTER_CUBIC);
			resize(right, needle[1][n], Size(), sl[n], sl[n], INTER_CUBIC);
		}
		img[0] = left.clone();	img[1] = right.clone();
		dstimg[0] = dst_left.clone();	dstimg[1] = dst_right.clone();
	}
	if (flg == "right")
	{
		for (int n = 0; n < 8; ++n)
		{
			sl[n] = pow(0.75, n);
			resize(right, needle[0][n], Size(), sl[n], sl[n], INTER_CUBIC);
			resize(left, needle[1][n], Size(), sl[n], sl[n], INTER_CUBIC);
		}
		img[0] = right.clone();	img[1] = left.clone();
		dstimg[0] = dst_right.clone();	dstimg[1] = dst_left.clone();
	}
#pragma omp parallel for
	for (int i = 0; i < height; ++i)
		for (int j = 0; j < width; ++j)
		{
			int d = disp.at<uchar>(i, j);
			double w_sum = 0;
			Mat z;
			vector<pt> patch_group;
			int pWin = min(ka, min(width - 1 - j, min(height - 1 - i, min(i, j))));
			int simin = max(i - Sa, pWin);
			int sjmin = max(j - Sa, pWin);
			int simax = min(i + Sa, height - 1 - pWin);
			int sjmax = min(j + Sa, width - 1 - pWin);
			if (pWin == ka)
				z = Mat::zeros(k, k, CV_32F);
			else
				z = Mat::zeros(2 * pWin + 1, 2 * pWin + 1, CV_32F);

#pragma omp parallel for
			for (int si = simin; si <= simax; ++si)
				for (int sj = sjmin; sj <= sjmax; ++sj)
				{
					double dist = 0.0;
					double dist_needle = 0.0;
					int img_num = 2;

					vector<Mat> ref_needle(8);
					vector<Mat> searc_needle(8);

					//needle simil
					int dist0 = 0.0;
					int s_num = 8;
					for (int n = 0; n < 8; ++n)
					{
						int sli = round(sl[n] * i);
						int slj = round(sl[n] * j);
						int sli2 = round(sl[n] * si);
						int slj2 = round(sl[n] * sj);
						int shei = round(sl[n] * height);
						int swid = round(sl[n] * width);
						if (sli + pWin + 1 >= shei || slj + pWin + 1 >= swid || sli2 + pWin + 1 >= shei || slj2 + pWin + 1 >= swid)
						{
							s_num--;
							continue;
						}
						ref_needle[n] = needle[0][n](Rect(max(0, slj - pWin), max(0, sli - pWin), 2 * pWin + 1, 2 * pWin + 1));
						searc_needle[n] = needle[0][n](Rect(max(0, slj2 - pWin), max(0, sli2 - pWin), 2 * pWin + 1, 2 * pWin + 1));
						dist0 += norm(ref_needle[n], searc_needle[n], NORM_L2SQR);
					}
					dist_needle /= max(1, s_num);
					//PCA simil
					for (int cj = 0; cj < 2; ++cj)
					{
						int ds_x;
						if (flg == "left")
							ds_x = (0 - cj)*d;
						if (flg == "right")
							ds_x = cj * d;

						//PCA simil
						if (i + pWin + 1 >= height || j + ds_x + pWin + 1 >= width ||
							si + pWin + 1 >= height || sj + ds_x + pWin + 1 >= width)
						{
							img_num--;
							continue;
						}
						Mat ref(img[cj], Rect(max(0,j + ds_x - pWin), max(0,i - pWin), 2 * pWin + 1, 2 * pWin + 1));
						Mat searc(img[cj], Rect(max(0,sj + ds_x - pWin), max(0,si - pWin), 2 * pWin + 1, 2 * pWin + 1));
						ref.convertTo(ref, CV_32F);
						searc.convertTo(searc, CV_32F);
						Mat dstref, dstsec;
						if (pWin == ka)
						{
							dstref = dstimg[cj].row(max(0,i - ka)*(width - 2 * ka) + max(0,j - ka + ds_x));
							dstsec = dstimg[cj].row(max(0,si - ka)*(width - 2 * ka) + max(0,sj - ka + ds_x));
						}
						else
						{
							dstref = ref;
							dstsec = searc;
						}
						dist += norm(dstref, dstsec, NORM_L2SQR);
					}
					dist /= img_num;
					dist += dist_needle;

					pt pt_el;
					pt_el.x = sj;
					pt_el.y = si;
					pt_el.dist = dist;
					if (patch_group.size() < clsnum)
						patch_group.push_back(pt_el);
					else
					{
						sort(patch_group.begin(), patch_group.end(), SortByDist);
						if (pt_el.dist < patch_group[0].dist)
							patch_group[0] = pt_el;
					}
				}

			/**************************************************************************************************/
			Mat PAT(patch_group.size() * 2, (2 * pWin + 1)*(2 * pWin + 1), CV_32F);
			for (int g = 0; g < patch_group.size(); ++g)
			{
				int xx = patch_group[g].x;
				int yy = patch_group[g].y;
				Mat pat = Mat(img[0], Rect(xx - pWin, yy - pWin, 2 * pWin + 1, 2 * pWin + 1));
				Mat pat_t;
				pat.convertTo(pat_t, CV_32F);
				pat_t.reshape(0, 1).copyTo(PAT.row(g));
			}
			for (int g = patch_group.size(); g < patch_group.size() * 2; ++g)
			{
				int xx = patch_group[g - patch_group.size()].x - d;
				int yy = patch_group[g - patch_group.size()].y;
				Mat pat = Mat(img[1], Rect(max(0, xx - pWin), yy - pWin, 2 * pWin + 1, 2 * pWin + 1));
				Mat pat_t;
				pat.convertTo(pat_t, CV_32F);
				pat_t.reshape(0, 1).copyTo(PAT.row(g));
			}
			PCA pca_s(PAT, Mat(), CV_PCA_DATA_AS_ROW, PCANum*2);
			PAT.release();

			Mat ref;
			if (pWin == ka)
			{
				Mat pat_tmp = Mat(img[0], Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
				Mat pat_tmp2;
				pat_tmp.convertTo(pat_tmp2, CV_32F);
				ref = pca_s.project(pat_tmp2.reshape(0, 1));
			}
			else
			{
				ref = Mat(img[0], Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
				ref.convertTo(ref, CV_32F);
			}

			for (int cj = 0; cj < 2; cj++)
			{
				int ds_x;
				if (flg == "left")
					ds_x = (0 - cj)*d;
				if (flg == "right")
					ds_x = cj * d;
				int center_x = j + ds_x;
				int center_y = i;
				if (center_x<0 || center_x>width - 1 || center_y<0 || center_y>height - 1)
					continue;
				for (int g = 0; g < patch_group.size(); ++g)
				{
					int step_x = j - patch_group[g].x;
					int step_y = i - patch_group[g].y;
					Mat pat;
					if (center_x - step_x - pWin<0 || center_y - step_y - pWin<0 || center_x - step_x + pWin + 1>width - 1 || center_y - step_y + pWin + 1>height - 1)
						continue;
					Mat pat0 = Mat(img[cj], Rect(center_x - step_x - pWin, center_y - step_y - pWin, 2 * pWin + 1, 2 * pWin + 1));
					pat0.convertTo(pat0, CV_32F);
					if (pWin == ka)
					{
						if (center_x - step_x - ka<0 || center_x - step_x - ka>width - 2 * ka - 1 || center_y - step_y - ka<0 || center_y - step_y - ka>height - 2 * ka - 1)
							continue;
						Mat pat_tmp;
						pat0.convertTo(pat_tmp, CV_32F);
						pat = pca_s.project(pat_tmp.reshape(0, 1));
					}
					else
					{
						pat = pat0;
						pat.convertTo(pat, CV_32F);
					}
					double dist = norm(ref, pat, NORM_L2SQR);
					double w = exp(-dist / (h*h));
					w_sum += w;
					z += w*pat0;
				}
			}
			/**************************************************************************************************/

			if (w_sum < 1e-9)
				continue;
			else
				z /= w_sum;
			Mat ROInum = num(Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
			cv::addWeighted(ROInum, 1, z, 1, 0, ROInum);
			Mat ROIden = den(Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
			cv::addWeighted(ROIden, 1, Mat::ones(z.rows, z.cols, z.type()), 1, 0, ROIden);
		}
	Mat tmp(height, width, CV_32F);
	cv::divide(num, den, tmp); tmp.convertTo(est, est.type());
	return est;
}
Mat NLM_multiv_PCA_2v_intera(Mat left, Mat right, Mat disp,Mat left_n,Mat right_n, int k, int S, int sigma, int clsnum, int PCANum, string flg)
{
	int height = left.rows;
	int width = left.cols;
	int ka = (k - 1) / 2;
	int Sa = (S - 1) / 2;
	double h = 2 * sigma + 18;
	Mat est = Mat::zeros(height, width, left.type());
	Mat den = Mat::zeros(height, width, CV_32F);
	Mat num = Mat::zeros(height, width, CV_32F);

	Mat PatAsRow((height - 2 * ka)*(width - 2 * ka) * 2, k*k, CV_32F);
	for (int i = ka; i < height - ka; ++i)
		for (int j = ka; j < width - ka; ++j)
		{
			Mat pat(left, Rect(j - ka, i - ka, k, k));
			pat.convertTo(pat, CV_32F);
			Mat pat_t = pat.clone();
			pat_t.reshape(0, 1).copyTo(PatAsRow.row((i - ka)*(width - 2 * ka) + j - ka));
		}
	for (int i = ka; i < height - ka; ++i)
		for (int j = ka; j < width - ka; ++j)
		{
			Mat pat(right, Rect(j - ka, i - ka, k, k));
			pat.convertTo(pat, CV_32F);
			Mat pat_t = pat.clone();
			pat_t.reshape(0, 1).copyTo(PatAsRow.row((height - 2 * ka)*(width - 2 * ka) + (i - ka)*(width - 2 * ka) + j - ka));
		}
	PCA pca(PatAsRow, Mat(), CV_PCA_DATA_AS_ROW, PCANum);

	/*****************************************************************************************************/
	Mat leftPAR, rightPAR;
	Mat dst_left, dst_right;

	leftPAR = Mat::zeros((height - 2 * ka)*(width - 2 * ka), k*k, CV_32F);
	dst_left = Mat::zeros((height - 2 * ka)*(width - 2 * ka), PCANum, CV_32F);
	rightPAR = Mat::zeros((height - 2 * ka)*(width - 2 * ka), k*k, CV_32F);
	dst_right = Mat::zeros((height - 2 * ka)*(width - 2 * ka), PCANum, CV_32F);

	for (int i = ka; i < height - ka; ++i)
		for (int j = ka; j < width - ka; ++j)
		{
			Mat pat(left, Rect(j - ka, i - ka, k, k));
			pat.convertTo(pat, CV_32F);
			Mat pat_t = pat.clone();
			pat_t.reshape(0, 1).copyTo(leftPAR.row((i - ka)*(width - 2 * ka) + j - ka));
			Mat pat2(right, Rect(j - ka, i - ka, k, k));
			pat2.convertTo(pat2, CV_32F);
			Mat pat_t2 = pat2.clone();
			pat_t2.reshape(0, 1).copyTo(rightPAR.row((i - ka)*(width - 2 * ka) + j - ka));
		}
	dst_left = pca.project(leftPAR);
	dst_right = pca.project(rightPAR);
	/*****************************************************************************************************/

	vector<Mat> img(2), dstimg(2),imgn(2);
	if (flg == "left")
	{
		img[0] = left.clone();	img[1] = right.clone();
		dstimg[0] = dst_left.clone();	dstimg[1] = dst_right.clone();
		imgn[0] = left_n.clone(); imgn[1] = right_n.clone();
	}
	if (flg == "right")
	{
		img[0] = right.clone();	img[1] = left.clone();
		dstimg[0] = dst_right.clone();	dstimg[1] = dst_left.clone();
		imgn[0] = right_n.clone(); imgn[1] = left_n.clone();
	}
#pragma omp parallel for
	for (int i = 0; i < height; ++i)
		for (int j = 0; j < width; ++j)
		{
			int d = disp.at<uchar>(i, j);
			double w_sum = 0;
			Mat z;
			vector<pt> patch_group;
			int pWin = min(ka, min(width - 1 - j, min(height - 1 - i, min(i, j))));
			int simin = max(i - Sa, pWin);
			int sjmin = max(j - Sa, pWin);
			int simax = min(i + Sa, height - 1 - pWin);
			int sjmax = min(j + Sa, width - 1 - pWin);
			if (pWin == ka)
				z = Mat::zeros(k, k, CV_32F);
			else
				z = Mat::zeros(2 * pWin + 1, 2 * pWin + 1, CV_32F);

#pragma omp parallel for
			for (int si = simin; si <= simax; ++si)
				for (int sj = sjmin; sj <= sjmax; ++sj)
				{
					double dist = 0.0;
					int img_num = 2;

					for (int cj = 0; cj < 2; ++cj)
					{
						int ds_x;
						if (flg == "left")
							ds_x = (0 - cj)*d;
						if (flg == "right")
							ds_x = cj * d;

						if (i - pWin < 0 || i + pWin + 1 >= height || j + ds_x - pWin < 0 || j + ds_x + pWin + 1 >= width ||
							i - (i - si) - pWin < 0 || i - (i - si) + pWin + 1 >= height || j + ds_x - (j - sj) - pWin < 0 || j + ds_x - (j - sj) + pWin + 1 >= width)
						{
							img_num--;
							continue;
						}

						Mat ref(img[cj], Rect(j + ds_x - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
						Mat searc(img[cj], Rect(j + ds_x - (j - sj) - pWin, i - (i - si) - pWin, 2 * pWin + 1, 2 * pWin + 1));
						ref.convertTo(ref, CV_32F);
						searc.convertTo(searc, CV_32F);
						Mat dstref, dstsec;
						if (pWin == ka)
						{
							dstref = dstimg[cj].row((i - ka)*(width - 2 * ka) + j - ka + ds_x);
							dstsec = dstimg[cj].row((i - ka - (i - si))*(width - 2 * ka) + j - ka + ds_x - (j - sj));
						}
						else
						{
							dstref = ref;
							dstsec = searc;
						}

						dist += norm(dstref, dstsec, NORM_L2SQR);
					}
					dist /= img_num;

					pt pt_el;
					pt_el.x = sj;
					pt_el.y = si;
					pt_el.dist = dist;
					if (patch_group.size() < clsnum)
						patch_group.push_back(pt_el);
					else
					{
						sort(patch_group.begin(), patch_group.end(), SortByDist);
						if (pt_el.dist < patch_group[0].dist)
							patch_group[0] = pt_el;
					}
				}

			/**************************************************************************************************/
			////show the selected patches
			//if (i == sh_i && j == sh_j)
			//{
			//	Mat tmpimg;
			//	vector<Mat> channels(3);
			//	channels[0] = left.clone();
			//	channels[1] = left.clone();
			//	channels[2] = left.clone();
			//	merge(channels, tmpimg);
			//	tmpimg.convertTo(tmpimg, CV_8UC3);
			//	label_rect(tmpimg, patch_group, ka, j, i);
			//	imshow("label", tmpimg); waitKey();
			//}

			Mat PAT(patch_group.size() * 2, (2 * pWin + 1)*(2 * pWin + 1), CV_32F);
			for (int g = 0; g < patch_group.size(); ++g)
			{
				int xx = patch_group[g].x;
				int yy = patch_group[g].y;
				Mat pat = Mat(img[0], Rect(xx - pWin, yy - pWin, 2 * pWin + 1, 2 * pWin + 1));
				Mat pat_t;
				pat.convertTo(pat_t, CV_32F);
				pat_t.reshape(0, 1).copyTo(PAT.row(g));
			}
			for (int g = patch_group.size(); g < patch_group.size() * 2; ++g)
			{
				int xx = patch_group[g - patch_group.size()].x - d;
				int yy = patch_group[g - patch_group.size()].y;
				Mat pat = Mat(img[1], Rect(max(0, xx - pWin), yy - pWin, 2 * pWin + 1, 2 * pWin + 1));
				Mat pat_t;
				pat.convertTo(pat_t, CV_32F);
				pat_t.reshape(0, 1).copyTo(PAT.row(g));
			}
			PCA pca_s(PAT, Mat(), CV_PCA_DATA_AS_ROW, PCANum*2);

			Mat ref;
			if (pWin == ka)
			{
				Mat pat_tmp = Mat(img[0], Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
				Mat pat_tmp2;
				pat_tmp.convertTo(pat_tmp2, CV_32F);
				ref = pca_s.project(pat_tmp2.reshape(0, 1));
			}
			else
			{
				ref = Mat(img[0], Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
				ref.convertTo(ref, CV_32F);
			}

			for (int cj = 0; cj < 2; cj++)
			{
				int ds_x;
				if (flg == "left")
					ds_x = (0 - cj)*d;
				if (flg == "right")
					ds_x = cj * d;
				int center_x = j + ds_x;
				int center_y = i;
				if (center_x<0 || center_x>width - 1 || center_y<0 || center_y>height - 1)
					continue;
				for (int g = 0; g < patch_group.size(); ++g)
				{
					int step_x = j - patch_group[g].x;
					int step_y = i - patch_group[g].y;
					Mat pat;
					if (center_x - step_x - pWin<0 || center_y - step_y - pWin<0 || center_x - step_x + pWin + 1>width - 1 || center_y - step_y + pWin + 1>height - 1)
						continue;
					Mat pat0 = Mat(img[cj], Rect(center_x - step_x - pWin, center_y - step_y - pWin, 2 * pWin + 1, 2 * pWin + 1));
					pat0.convertTo(pat0, CV_32F);
					if (pWin == ka)
					{
						if (center_x - step_x - ka<0 || center_x - step_x - ka>width - 2 * ka - 1 || center_y - step_y - ka<0 || center_y - step_y - ka>height - 2 * ka - 1)
							continue;
						Mat pat_tmp;
						pat0.convertTo(pat_tmp, CV_32F);
						pat = pca_s.project(pat_tmp.reshape(0, 1));
					}
					else
					{
						pat = pat0;
						pat.convertTo(pat, CV_32F);
					}
					double dist = norm(ref, pat, NORM_L2SQR);
					double w = exp(-dist / (h*h));
					w_sum += w;
					Mat pat0n = Mat(imgn[cj], Rect(center_x - step_x - pWin, center_y - step_y - pWin, 2 * pWin + 1, 2 * pWin + 1));
					pat0n.convertTo(pat0n, CV_32F);
					z += w*pat0n;
				}
			}
			/**************************************************************************************************/

			if (w_sum < 1e-9)
				continue;
			else
				z /= w_sum;
			Mat ROInum = num(Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
			cv::addWeighted(ROInum, 1, z, 1, 0, ROInum);
			Mat ROIden = den(Rect(j - pWin, i - pWin, 2 * pWin + 1, 2 * pWin + 1));
			cv::addWeighted(ROIden, 1, Mat::ones(z.rows, z.cols, z.type()), 1, 0, ROIden);
		}
	Mat tmp(height, width, CV_32F);
	cv::divide(num, den, tmp); tmp.convertTo(est, est.type());
	return est;
}

double getPSNR(Mat I1, Mat I2)
{
	Mat e;
	int k = 7;
	absdiff(I1(Rect(k+1, k+1, I1.cols - 2*k, I1.rows - 2*k)), I2(Rect(k + 1, k + 1, I1.cols - 2 * k, I1.rows - 2 * k)), e);
	double mse;
	if (I1.channels() == 1)
	{
		if (e.type() != CV_32F)
			e.convertTo(e, CV_32F);
		e = e.mul(e);
		mse = mean(e).val[0];
	}
	else
	{
		if (e.type() != CV_32FC3)
			e.convertTo(e, CV_32FC3);
		e = e.mul(e);
		mse = (mean(e).val[0]+ mean(e).val[1]+ mean(e).val[2])/3.0;
	}
	double psnr = 10.0*log10((255 * 255) / mse);

	return psnr;
}
void main()
{
	int k = 7;//patch size
	int S = 19;//window size
	int sigma = 25;//noise level
	int clsnum = 18;//number of patches
	int PCANum = 3;//number of principal components
	
	cout << "load images and initialization.." << endl;
	Mat left = imread("imL4.png");
	Mat right = imread("imR4.png");
	if (!left.data || !right.data)
	{
		cout << "load image error." << endl;
		getchar();
		return;
	}
	if (left.channels() == 3 || right.channels() == 3)
	{
		cvtColor(left, left, CV_BGR2GRAY);
		cvtColor(right, right, CV_BGR2GRAY);
	}

	int dr = 59, scale = 255 / dr;//disparity range and corresponding scale
	int height = left.rows;
	int width = left.cols;
	Mat tmp_left, noise_left;
	left.convertTo(tmp_left, CV_32F);
	Mat GN(tmp_left.size(), tmp_left.type());
	randn(GN, 0.f, float(sigma));
	noise_left = tmp_left + GN;
	Mat tmp_right, noise_right;
	right.convertTo(tmp_right, CV_32F);
	noise_right = tmp_right + GN;
	
	//get initial disparities (AD+BoxFilt)
	cout << "compute initial disparity.." << endl;
	Mat left_8, right_8;
	noise_left.convertTo(left_8, CV_8U);
	noise_right.convertTo(right_8, CV_8U);
	vector<Mat> cost = AD(left_8, right_8, dr);
	Mat disp = AggrBox(cost, 7);
	vector<Mat> cost_r = AD_r2l(left_8, right_8, dr);
	Mat disp_r = AggrBox(cost_r, 7);
	
	//processing (prop.)
	cout << "processing.." << endl;
	Mat NLMMtiPCA_l = NLM_multiv_PCA_2v_needle(noise_left, noise_right, disp, k, S, sigma, clsnum, PCANum, "left");
	NLMMtiPCA_l.convertTo(NLMMtiPCA_l, CV_8U);
	Mat NLMMtiPCA_r = NLM_multiv_PCA_2v_needle(noise_left, noise_right, disp_r, k, S, sigma, clsnum, PCANum, "right");
	NLMMtiPCA_r.convertTo(NLMMtiPCA_r, CV_8U);
	cost = AD(NLMMtiPCA_l, NLMMtiPCA_r, dr);
	disp = AggrBox(cost, 7);
	Mat NLMMtiPCA_out = NLM_multiv_PCA_2v_intera(NLMMtiPCA_l, NLMMtiPCA_r, disp, noise_left, noise_right, k, S, sigma, clsnum, PCANum, "left");
	
	//show result
	cout << "output.." << endl;
	Mat clean = left.clone();
	Mat noise = noise_left.clone(); noise.convertTo(noise, CV_8U);
	NLMMtiPCA_out.convertTo(NLMMtiPCA_out, CV_8U);
	imshow("noisy", noise);
	imshow("output", NLMMtiPCA_out);
	cout << "PSNR (clean|NL-Means multiview PCA): " << getPSNR(clean, NLMMtiPCA_out) << endl;
	waitKey();
}