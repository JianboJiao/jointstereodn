


# Joint Image Denoising and Disparity Estimation via Stereo Structure PCA and Noise-Tolerant Cost

By [Jianbo Jiao](http://www.cs.cityu.edu.hk/~jianbjiao2/), [Qingxiong Yang](https://sites.google.com/site/itrymanytimes/home), [Shengfeng He](http://www.shengfenghe.com/), [Shuhang Gu](https://sites.google.com/site/shuhanggu/home), [Lei Zhang](http://www4.comp.polyu.edu.hk/~cslzhang/), [Rynson W. H. Lau](http://www.cs.cityu.edu.hk/~rynson/)


### Introduction

This repository contains the main codes that implement the algorithm described in the paper "[Joint Image Denoising and Disparity Estimation via Stereo Structure PCA and Noise-Tolerant Cost](http://www.cs.cityu.edu.hk/~jianbjiao2/pdfs/ijcv.pdf)" . This work jointly tackles the problems of image denoising and disparity estimation from stereo images.

### Usage

This code is a Visual Studio Project on Windows x64 platform with OpenCV.

0. To build the project, please install and configure [OpenCV](https://opencv.org/) on your PC. The current code has been tested on Visual Studio 2017 and OpenCV3.4. Other versions should also work, but not guaranteed.
1. Download this repository.
2. The code [main.cpp](Demo/main.cpp) is a demo code to illustrate the algorithm, please run for test.

### Citation

If you find these codes useful in your research, please cite:

	@article{jiao2017joint,
	  title={Joint Image Denoising and Disparity Estimation via Stereo Structure PCA and Noise-Tolerant Cost},
	  author={Jiao, Jianbo and Yang, Qingxiong and He, Shengfeng and Gu, Shuhang and Zhang, Lei and Lau, Rynson WH},
	  journal={International Journal of Computer Vision},
	  pages={1--19},
	  year={2017},
	  publisher={Springer US}
	}


This code is for research useage only. If you have any questions please feel free to contact the [authors](mailto:jiaojianbo.i@gmail.com)